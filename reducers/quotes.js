import * as types from '../constants/actionTypes';

const INITIAL_STATE = {
  quotesList: []
};

//Implement the reducer
export function quotes(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.ADD_QUOTE:
      return {
        ...state,
        quotesList: [
          action.value,
          ...state.quotesList
        ]
      };
    default:
      return state
  }
}