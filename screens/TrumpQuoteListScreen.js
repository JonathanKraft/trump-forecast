import React from "react";
import {
  View,
  Text,
  ImageBackground,
} from "react-native";
import QuoteList from '../containers/QuoteList'

export default class TrumpQuoteListScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};

    return {
      header: null
    };
  };

  goToSingleQuote = (quote) => {
    this.props.navigation.navigate('TrumpQuoteDetail', { quote })
  };

  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ImageBackground
          style={{ width: "100%", height: "100%", position: "absolute" }}
          source={require("../assets/image/trump-on-wind2.jpg")}
        />
        <Text
          style={{
            color: "#ffffff",
            fontWeight: "bold",
            textAlign: "center",
            fontFamily: "Montserrat-Regular",
            fontSize: 44,
            marginTop: 50
          }}
        >
          Donald Trump
      </Text>
        <Text
          style={{
            color: "#ffffff",
            fontStyle: "italic",
            textAlign: "center",
            fontFamily: "Montserrat-Regular",
            fontSize: 22
          }}
        >
          un jour à dit ...
      </Text>

        <QuoteList onQuotePress={this.goToSingleQuote}/>

      </View>
    );
  }
}
