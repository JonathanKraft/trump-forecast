import React from 'react';
import Single from '../components/quotes/Single';

export default class TrumpQuoteDetailScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  }

  render() {
    return (
     <Single 
     quote={this.props.navigation.getParam('quote')}
     />
    );
  }
}