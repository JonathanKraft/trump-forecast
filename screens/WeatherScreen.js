import React from 'react';
import { View, Text, Button } from 'react-native';

export default class WeatherScreen extends React.Component {
  
  static navigationOptions = ({ navigation }) => {
      return {
          header: null,
      };
  }
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Weather Screen</Text>
      </View>
    );
  }
}