import React from 'react';
import { ImageBackground, TouchableOpacity, View, Text } from 'react-native';
import ImageSvg from 'react-native-remote-svg';

export default class HomeScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            header: null,
        };
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ImageBackground
                    style={{ width: '100%', height: '100%', position: "absolute" }}
                    source={require('../assets/image/trump-on-wind.png')}
                />
                <Text style={{ color: '#ffffff', fontWeight: "bold", textAlign: "center", fontFamily: "Montserrat-Regular", fontSize: 44 }}>Hello World !</Text>
                <Text style={{ color: '#ffffff', fontStyle: "italic", textAlign: "center", fontFamily: "Montserrat-Regular", fontSize: 22 }}>Choose your category</Text>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('Weather')}>

                    <View style={{ marginTop: 10, marginBottom: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFFFFF', opacity: 0.9, width: 336, height: 160, borderRadius: 12 }}>
                        <ImageSvg
                            source={require('../assets/image/Shades.svg')}


                        />
                        <Text style={{ color: '#404491', fontWeight: "bold", fontStyle: "italic", textAlign: "center", fontFamily: "Montserrat-Regular", fontSize: 22 }}>WEATHER FORECAST</Text>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('TrumpQuoteList')}>
                    <View style={{ marginTop: 10, marginBottom: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255,255,255,0.9)', width: 336, height: 160, borderRadius: 12 }}>
                        <ImageSvg
                            source={require('../assets/image/djt-icon.svg')}

                        />
                        <Text style={{ color: '#404491', fontWeight: "bold", fontStyle: "italic", textAlign: "center", fontFamily: "Montserrat-Regular", fontSize: 22 }}>TRUMP'S QUOTES</Text>
                    </View>
                </TouchableOpacity>
            </View>

        );
    }
}