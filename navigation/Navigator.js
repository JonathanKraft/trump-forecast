import React from 'react';
import { View, Text, Button } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';


import HomeStack from './stacks/HomeStack';
import TrumpQuoteListStack from './stacks/TrumpQuoteListStack';
import WeatherStack from './stacks/WeatherStack';


import Ionicons from 'react-native-vector-icons/Ionicons';


const BottomBar =  createBottomTabNavigator(
  {
    Home: HomeStack,
    Weather: WeatherStack,
    TrumpQuoteList: TrumpQuoteListStack,
  },
  {
    navigationOptions: ({ navigation }) => ({
    }),
    tabBarOptions: {
      activeTintColor: '#404491',
      inactiveTintColor: '#707070',
        tabStyle: {
            borderLeftWidth:1,
            marginTop: 10,
            marginBottom: 10,
           
            
        },
        labelStyle:{fontWeight:"bold", fontSize: 20, justifyContent: 'center'}
    },    
  }
);


const AppContainer = createAppContainer(BottomBar);
export default AppContainer;