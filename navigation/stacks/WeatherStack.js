import React from 'react';
import { View, Text, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';

import WeatherScreen from '../../screens/WeatherScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';

const WeatherStack = createStackNavigator(
    {
        Weather: WeatherScreen,
    },
);

WeatherStack.navigationOptions = {
    tabBarLabel: 'Weather',
    // tabBarIcon: ({ focused }) => {
    //     let iconName = `ios-information-circle${focused ? '' : '-outline'}`;
    //     return <Ionicons name={iconName} size={25} color='tomato'/>
    // },
};

export default WeatherStack;