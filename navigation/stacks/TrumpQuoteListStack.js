import React from 'react';
import { View, Text, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import Ionicons from 'react-native-vector-icons/Ionicons';



import TrumpQuoteListScreen from '../../screens/TrumpQuoteListScreen';
import TrumpQuoteDetailScreen from '../../screens/TrumpQuoteDetailScreen';


const TrumpQuoteListStack = createStackNavigator(
    {
        TrumpQuoteList: TrumpQuoteListScreen,
        TrumpQuoteDetail: TrumpQuoteDetailScreen,
    },
);

TrumpQuoteListStack.navigationOptions = {
    tabBarLabel: 'Trump',
    // tabBarIcon: ({ focused }) => {
    //     let iconName = `ios-cafe${focused ? '' : '-outline'}`;
    //     return <Ionicons name={iconName} size={25} color='tomato'/>
    // },
};

export default TrumpQuoteListStack;