import React from 'react';
import { View, Text, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../../screens/HomeScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';

const HomeStack = createStackNavigator(
    {
        Home: HomeScreen,
    },
);

HomeStack.navigationOptions = {
    tabBarLabel: 'Home',
    // tabBarIcon: ({ focused }) => {
        
    //     let iconName = `ios-information-circle${focused ? '' : '-outline'}`;
    //     return <Ionicons name={iconName} size={25} color='tomato'/>
    // },
};

export default HomeStack;