import React from 'react';
import * as Font from 'expo-font';
import Navigator from './navigation/Navigator.js';
import { Provider } from 'react-redux';
import store from './store';


export default class App extends React.Component {
  state = {
    fontLoaded: false,
  };

  async componentDidMount() {
    await Font.loadAsync({
      'Montserrat-Regular': require('./assets/fonts/Montserrat-Regular.ttf'),
      'Montserrat-Bold': require('./assets/fonts/Montserrat-Bold.ttf'),
      'Montserrat-Italic': require('./assets/fonts/Montserrat-RegularItalic.ttf'),
      'Montserrat-bold-italic': require('./assets/fonts/Montserrat-ExtraBoldItalic.ttf'),
    });
    this.setState({ fontLoaded: true });
  }
  render() {
    return (
      this.state.fontLoaded ? (
        <Provider store={store}>
          <Navigator />
        </Provider >
      ) : <></>
    )
  }
}