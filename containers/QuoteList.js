import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import QuoteList from '../components/quotes/QuoteList';
import { fetchQuote } from '../actions/quotes'

const mapStateToProps = state => {
    return {
        quotesList: state.quotes.quotesList,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators({fetchQuote}, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(QuoteList);