import * as types from '../constants/actionTypes';

export const fetchQuote = () => {
  return async dispatch => {
      const response = await fetch('https://api.tronalddump.io/random/quote');
      const quote = await response.json();
    dispatch(addQuote(quote));
  }
};

export const addQuote = (quote) => {
  return { type: types.ADD_QUOTE, value: quote }
};