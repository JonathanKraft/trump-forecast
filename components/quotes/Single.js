import React from 'react';
import { View, Text, Button, ImageBackground } from 'react-native';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import * as SMS from 'expo-sms';


export default class Single extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  }



  render() {
    const { quote } = this.props;

    const date = moment(quote.appeared_at).format('DD/MM/YYYY');
    const hour = moment(quote.appeared_at).format('H');
    const minutes = moment(quote.appeared_at).format('mm');

    sendMessage = async () => {
      const sms = await SMS.sendSMSAsync([], `Donald Trump a dit : \n ${quote.value}. \n\nCe message a été envoyé via mon téléphone de ouf.`);
      if (sms.result === 'sent') {
        Alert.alert('Succès !', 'Votre citation a bien été envoyée !')
      }
    };
    return (
      <ImageBackground
        source={require('../../assets/image/trump-on-wind2.jpg')}
        style={{ width: '100%', height: '100%', flex: 1 }}
      >
        <ImageBackground
          source={require('../../assets/image/trump-ok.png')}
          style={{ width: '100%', height: '40%', flex: 1, alignItems: 'center', justifyContent: 'flex-end', flexDirection: 'column' }}
        >
          <Ionicons
            name="md-arrow-round-back"
            style={{ position: "absolute", top: 30, left: 20, color: '#ffffff', fontSize: 50 }}
          // onPress={() => this.props.navigation.navigate('TrumpQuoteList')}
          />
          <View style={{ width: '85%', maxHeight: '70%', flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'space-between', padding: 10, margin: 10, backgroundColor: 'white', borderRadius: 12, shadowColor: '#00000029' }}>

            <Text
              style={{ fontFamily: 'Montserrat-Italic', color: '#707070', textAlign: 'center', fontSize: 16 }}>
              Le {date} à {hour}:{minutes}, il à dit:
              </Text>
            <ScrollView stype={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'space-evenly' }}>
              <Text
                style={{ fontFamily: 'Montserrat-bold-italic', color: '#404491', textAlign: 'center', fontSize: 26 }}>
                {quote.value}
              </Text>
            </ScrollView >

            <Text
              style={{ fontFamily: 'Montserrat-bold-italic', color: '#707070', textAlign: 'center', fontSize: 16 }}>
              #Weather #FakeNews</Text>
            <TouchableOpacity
              style={{ backgroundColor: '#404491', padding: 5, borderRadius: 12 }}
              onPress={sendMessage}
            >
              <View>
                <Text
                  style={{ fontFamily: 'Montserrat-bold-italic', color: '#FFFFFF', textAlign: 'center', fontSize: 20 }}>
                  ENVOYER LA CITATION à UN(E) AMI(E)</Text>
              </View>
            </TouchableOpacity>
          </View>

        </ImageBackground>
      </ImageBackground>
    );
  }
}