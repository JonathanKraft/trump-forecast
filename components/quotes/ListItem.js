import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from "react-native";
import { Icon } from "react-native-elements";

export default class ListItem extends React.Component {
  render() {
    return (
          <TouchableOpacity
            onPress={() => this.props.onPress(this.props.quote)}
          >
            <View
              style={{
                flex: 1,
                marginTop: 10,
                marginBottom: 10,
                alignItems: "center",
                backgroundColor: "#FFFFFF",
                opacity: 0.9,
                width: 336,
                borderRadius: 12
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                  alignItems: "center"
                }}
              >
                <View style={{ width: "25%", alignContent: "center" }}>
                  <Image
                    style={{ position: "relative", margin: 10 }}
                    source={require("../../assets/image/trump-el-paso-baby.png")}
                  />
                </View>
                <View style={{ width: "40%" }}>
                  <Text
                    style={{
                      color: "#404491",
                      fontStyle: "italic",
                      textAlign: "left",
                      fontFamily: "Montserrat-Regular",
                      fontSize: 16
                    }}
                  >
                    {this.props.quote.appeared_at}
                  </Text>
                  {/* <Text
                    style={{
                      color: "#404491",
                      fontStyle: "italic",
                      textAlign: "left",
                      fontFamily: "Montserrat-Regular",
                      fontSize: 16
                    }}
                  >
                    {this.props.quote.hour}
                  </Text> */}
                </View>
                <View
                  style={{ width: "15%", alignContent: "flex-start" }}
                ></View>
                <View style={{ width: "20%" }}>
                  <Icon
                    name="chevron-right"
                    size={40}
                    color="#333675"
                    // onPress={() => this.props.onPress(this.props.quote)}
                  />
                </View>
              </View>
              <View>
                <Text
                  style={{
                    color: "#404491",
                    fontWeight: "bold",
                    fontStyle: "italic",
                    textAlign: "left",
                    fontFamily: "Montserrat-Regular",
                    fontSize: 18,
                    padding: 15
                  }}
                >
                  {this.props.quote.value}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
    );
  }
}
