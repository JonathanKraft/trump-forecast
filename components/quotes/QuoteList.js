import React from 'react';
import ListItem from './ListItem';
import {
  ScrollView,
  RefreshControl
} from "react-native";

export default class QuoteList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  };

  componentDidMount = () => {
    this.props.actions.fetchQuote();
  }

  fetchQuote = () => {
    this.setState({ refreshing: true });
    this.props.actions.fetchQuote().then(() => {
      this.setState({ refreshing: false });
    });
  }

  renderQuotesItem = () => {
    return this.props.quotesList.map((item, index) => {
      return <ListItem
        key={index}
        quote={item}
        onPress={this.props.onQuotePress}
      />
    })
  };

  render() {
    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.fetchQuote}
          />
        }
      >
        {this.renderQuotesItem()}
      </ScrollView>
    )
  }

}